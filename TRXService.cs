﻿using Api.Core;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using TRX.Nosis.Coelsa.API.Configuration;
using TRX.Nosis.Coelsa.API.Services;

namespace TRX.Nosis.Coelsa.API
{
    public class TRXService : CoreService, IConfigureServicesAction, IConfigureWebApplication
    {
        public int Priority { get; } = 1;

        public void ConfigureServices(IServiceCollection services)
        {
            //throw new NotImplementedException();
            services.AddNosisConfiguration();
            services.AddCoelsaConfiguration();
            services.AddSingleton<INosisService, NosisService>();
            services.AddSingleton<ICoelsaService, CoelsaService>();
        }

        public void ConfigureWeb(IApplicationBuilder applicationBuilder, IWebHostEnvironment environment)
        {
            //throw new NotImplementedException();
        }
    }
}
