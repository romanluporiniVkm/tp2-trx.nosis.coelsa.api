﻿using Newtonsoft.Json;
using TRX.Nosis.Coelsa.API.Models.Request;
using TRX.Nosis.Coelsa.API.Models.Response;

namespace TRX.Nosis.Coelsa.API.Helpers
{
    public class CuitCheckerHelper
    {
        public static string Check(string nosisResult, string coelsaResult, string cbuDebito, string cbuCredito)
        {
            var dNosisResult = JsonConvert.DeserializeObject<PersonaResponse>(nosisResult);
            var dCoelsaResult = JsonConvert.DeserializeObject<CBUResponse>(coelsaResult);

            if (dCoelsaResult.Titulares == null) return null;

            string cuitNosis, cuitCoelsa;

            foreach (var dato in dNosisResult.Datos)
            {
                if (dato.Detalle != null) return null;

                cuitNosis = dato.Valor;

                foreach (var titular in dCoelsaResult.Titulares)
                {
                    cuitCoelsa = titular.Cuit;

                    if (cuitNosis == cuitCoelsa) //llamar metodo para transferencia
                    {
                        var data = new TRXRequest
                        {
                            CuitDebito = cuitNosis,
                            CbuDebito = cbuDebito,
                            TitularDebito = titular.Nombre,
                            CuitCredito = cuitNosis,
                            CbuCredito = cbuCredito,
                            Concepto = "VAR",
                            Importe = 15,
                            Descripcion = "Prueba Aviso Crédito 1"
                        };

                        return JsonConvert.SerializeObject(data);
                    }
                }
            }

            return null;
        }

    }
}
