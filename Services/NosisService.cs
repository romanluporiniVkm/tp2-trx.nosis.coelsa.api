﻿using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TRX.Nosis.Coelsa.API.Models.Request;

namespace TRX.Nosis.Coelsa.API.Services
{
    public class NosisService : INosisService
    {
        public async Task<string> NosisClient(PersonaRequest nosisBody, int dni)
        {
            HttpClient httpClient = new HttpClient();

            Console.WriteLine("en el cliente nosis");

            var json = JsonConvert.SerializeObject(nosisBody);
            var data = new StringContent(json, Encoding.UTF8, "application/json");

            var response = await httpClient.PostAsync($"https://localhost:7001/GetInfoPersona/{dni}", data);

            string result = response.Content.ReadAsStringAsync().Result;

            return result;
        }
    }
}
