﻿using System.Threading.Tasks;
using TRX.Nosis.Coelsa.API.Models.Request;

namespace TRX.Nosis.Coelsa.API.Services
{
    public interface ICoelsaService
    {
        Task<string> CoelsaCBUClient(string cbu);
        Task<string> CoelsaTRXClient(TRXRequest TRXbody);
    }
}
