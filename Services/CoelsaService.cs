﻿using Newtonsoft.Json;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using TRX.Nosis.Coelsa.API.Models.Request;

namespace TRX.Nosis.Coelsa.API.Services
{
    public class CoelsaService : ICoelsaService
    {
        public async Task<string> CoelsaCBUClient(string cbu)
        {
            HttpClient httpClient = new HttpClient();

            var response = await httpClient.GetAsync($"https://localhost:8001/consulta/cbu/{cbu}");

            string result = response.Content.ReadAsStringAsync().Result;

            return result;
        }
        public async Task<string> CoelsaTRXClient(TRXRequest TRXbody)
        {
            HttpClient httpClient = new HttpClient();

            var json = JsonConvert.SerializeObject(TRXbody);
            var data = new StringContent(json, Encoding.UTF8, "application/json");

            var response = await httpClient.PostAsync($"https://localhost:8001/transferencia", data);

            string result = response.Content.ReadAsStringAsync().Result;

            return result;
        }
    }
}
