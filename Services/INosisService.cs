﻿using System.Threading.Tasks;
using TRX.Nosis.Coelsa.API.Models.Request;

namespace TRX.Nosis.Coelsa.API.Services
{
    public interface INosisService
    {
        Task<string> NosisClient(PersonaRequest nosisBody, int dni);
    }
}
