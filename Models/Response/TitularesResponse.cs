﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TRX.Nosis.Coelsa.API.Models.Response
{
    public class TitularesResponse
    {
        public string TipoPersona { get; set; }
        public string Cuit { get; set; }
        public string Nombre { get; set; }

    }
}
