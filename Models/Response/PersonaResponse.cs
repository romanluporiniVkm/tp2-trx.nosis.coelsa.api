﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TRX.Nosis.Coelsa.API.Models.Response
{
    public class PersonaResponse
    {
        public IList<DatosResponse> Datos { get; set; }
        public int IdPersonasVariables { get; set; }
    }
}
