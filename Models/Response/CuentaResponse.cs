﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TRX.Nosis.Coelsa.API.Models.Response
{
    public class CuentaResponse
    {
        public string TipoCuenta { get; set; }
        public string IdBanco { get; set; }
        public Boolean Activa { get; set; }
        public string Cbu { get; set; }
        public string CbuAnterior { get; set; }
        public string Alias { get; set; }
        public string AliasAnterior { get; set; }
    }
}
