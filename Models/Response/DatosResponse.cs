﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TRX.Nosis.Coelsa.API.Models.Response
{
    public class DatosResponse
    {
        public string Codigo { get; set; }
        public string Descripcion {get; set;}
        public string FechaActualizacion {get; set;}
        public string Detalle {get; set;}
        public string Valor {get; set;}
    } 
}
