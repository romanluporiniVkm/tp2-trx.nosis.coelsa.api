﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TRX.Nosis.Coelsa.API.Models.Response
{
    public class TRXEstadoResponse
    {
        public string Codigo { get; set; }
        public string Descripcion { get; set; }

        public string ErrorCoelsa { get; set; }
    }
}
