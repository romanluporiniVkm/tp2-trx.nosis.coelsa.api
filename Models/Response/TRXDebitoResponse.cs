﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TRX.Nosis.Coelsa.API.Models.Response
{
    public class TRXDebitoResponse
    {
        public int Id { get; set; }
        public string IdTrx { get; set; }

        public string Cbu { get; set; }

        public string Moneda { get; set; }

        public decimal Importe { get; set; }

        public string Cvu { get; set; }


    }
}
