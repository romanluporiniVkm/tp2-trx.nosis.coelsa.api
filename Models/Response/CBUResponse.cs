﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TRX.Nosis.Coelsa.API.Models.Response
{
    public class CBUResponse
    {
        public CuentaResponse Cuenta { get; set; }
        public IList<TitularesResponse> Titulares { get; set; }

    }
}
