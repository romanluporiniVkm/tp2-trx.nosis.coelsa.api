﻿namespace TRX.Nosis.Coelsa.API.Models.Response
{
    public class TRXResponse
    {
        public TRXDebitoResponse Debito { get; set; }
        public TRXEstadoResponse Estado { get; set; }
    }
}
