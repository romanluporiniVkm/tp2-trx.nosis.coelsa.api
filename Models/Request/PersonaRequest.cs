﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TRX.Nosis.Coelsa.API.Models.Request
{
    public class PersonaRequest
    {
        public string[] Fields { get; set; }
        [Required]
        public int IdNegocio { get; set; }
        [Required]
        public int IdTipoOperacion { get; set; }
        [Required]
        [MaxLength(1)]
        public string Sexo { get; set; }
    }
}
