﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TRX.Nosis.Coelsa.API.Models.Request
{
    public class TRXRequest
    {
        public string CuitDebito { get; set; }
        public string CbuDebito { get; set; }
        public string TitularDebito { get; set; }
        public string CuitCredito { get; set; }
        public string CbuCredito { get; set; }
        public string Concepto { get; set; }
        public decimal Importe { get; set; }
        public string Descripcion { get; set; }
    }
}
