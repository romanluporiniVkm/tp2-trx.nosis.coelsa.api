﻿using Api.Core.Configuration;
using DataProtectionOptions = Api.Core.Configuration.DataProtectionOptions;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.DataProtection;

namespace TRX.Nosis.Coelsa.API.Configuration
{
    public static class ServiceCollectionExtensions
    {
        public static void AddNosisConfiguration(this IServiceCollection services)
        {
            IConfiguration configuration;

            using (var serviceScope = services.BuildServiceProvider().CreateScope())
            {
                configuration = serviceScope.ServiceProvider.GetService<IConfiguration>();
            }

            var nosisOptions = new NosisOptions();
            configuration.GetSection(NosisOptions.Section).Bind(nosisOptions);

            services.AddSingleton(typeof(NosisOptions), nosisOptions);
        }

        public static void AddCoelsaConfiguration(this IServiceCollection services)
        {
            IConfiguration configuration;
            DataProtectionOptions dataProtectionOptions;

            using (var serviceScope = services.BuildServiceProvider().CreateScope())
            {
                configuration = serviceScope.ServiceProvider.GetService<IConfiguration>();
                dataProtectionOptions = serviceScope.ServiceProvider.GetService<DataProtectionOptions>();
            }

            var coelsaOptions = new CoelsaOptions();
            configuration.GetSection(CoelsaOptions.Section).Bind(coelsaOptions);

            if (dataProtectionOptions.MustDeserializeConfiguration)
            {
                IDataProtector dataProtector;
                using (var serviceScope = services.BuildServiceProvider().CreateScope())
                {
                    var dataProtectorProvider = serviceScope.ServiceProvider.GetService<IDataProtectionProvider>();
                    dataProtector = dataProtectorProvider.CreateProtector(dataProtectionOptions.DeserializerName);
                }

                coelsaOptions.Unprotect(dataProtector);
            }

            services.AddSingleton(typeof(CoelsaOptions), coelsaOptions);
        }
    }
}
