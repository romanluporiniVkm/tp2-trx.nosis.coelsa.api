﻿using Microsoft.AspNetCore.DataProtection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TRX.Nosis.Coelsa.API.Configuration
{
    public class CoelsaOptions
    {
        public string BaseUrl { get; set; }
        public string IdPsp { get; set; }
        public int MinutosDelayParaInformarTrxPendiente { get; set; }

        public static string Section = "Application:Banco:Coelsa";

        public void Unprotect(IDataProtector dataProtector)
        {
            IdPsp = dataProtector.Unprotect(IdPsp);
        }
    }
}
