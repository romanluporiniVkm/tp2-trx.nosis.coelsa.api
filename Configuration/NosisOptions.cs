﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TRX.Nosis.Coelsa.API.Configuration
{
    public class NosisOptions
    {
        public string BaseUrl { get; set; }
        public short IDNegocio { get; set; }
        public int TipoOperacion { get; set; }
        public string Sexo { get; set; }
        public string[] Fields { get; set; }


        public static string Section = "Application:Banco:Nosis";
    }
}
