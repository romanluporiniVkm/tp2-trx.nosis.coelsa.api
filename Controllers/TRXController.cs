﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Threading.Tasks;
using TRX.Nosis.Coelsa.API.Helpers;
using TRX.Nosis.Coelsa.API.Models.Request;
using TRX.Nosis.Coelsa.API.Models.Response;
using TRX.Nosis.Coelsa.API.Services;

namespace TRX.Nosis.Coelsa.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TRXController : ControllerBase
    {
        private readonly INosisService _nosisService;
        private readonly ICoelsaService _coelsaService;

        public TRXController(ICoelsaService coelsaService,
            INosisService nosisService)
        {
            _coelsaService = coelsaService;
            _nosisService = nosisService;
        }

        [HttpPost]
        [Route("{dni}/{cbuDebito}/{cbuCredito}")]
        public async Task<IActionResult> ValidateAndTransferAsync([FromBody] PersonaRequest nosisBody, int dni, string cbuDebito, string cbuCredito)
        {
            var nosisResult = await _nosisService.NosisClient(nosisBody, dni);
            var coelsaResult = await _coelsaService.CoelsaCBUClient(cbuDebito);

            //con la respuesta de ambos servicios checkeamos
            //cuit de origen NOSIS con
            //cuit de origen en COELSA
            var validatedData = CuitCheckerHelper.Check(nosisResult, coelsaResult, cbuDebito, cbuCredito);

            if (validatedData != null)
            {
                var dValidatedData = JsonConvert.DeserializeObject<TRXRequest>(validatedData);
                var tRXResponse = await _coelsaService.CoelsaTRXClient(dValidatedData);
                var dTRXResponse = JsonConvert.DeserializeObject<TRXResponse>(tRXResponse);

                if (dTRXResponse.Estado == null)
                {
                    return BadRequest("Hay algun error con los datos ingresados");
                }

                return Ok(dTRXResponse);
            }
            else
            {
                return BadRequest("Hay algun error con los datos ingresados");
            }
        }
    }
}
